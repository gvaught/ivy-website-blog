import { Link } from "gatsby"
import React, { Component } from "react"
import instagramIcon from "../../images/icons/instagram-round-white-24px.svg"
import githubIcon from "../../images/icons/github-icon-24px.svg"
import styled from 'styled-components'
import styles from "./footer.module.scss"

const FooterDiv = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: 1fr;
  align-items: baseline;
  justify-items: center;
`
const Footernav = styled.ul`
  margin: 0 1rem;
  color: #fcfcfc;
  &:hover{color: #F2C9D6;}
`
const SelfPromo = styled.span`
display: block;
  font-size: .64rem;
  a {
    color: #F2C9D6;
    padding: 0; margin: 0;
    :hover {
      color: #fcfcfc;
      text-decoration: underline;
    }
  }
`
const Copyright = styled.div`
  display: block;
  font-size: .8rem;
  padding: 0;
  margin: 0;
  font-weight: 300;
`

class Footer extends Component {
  render() {
    return (
      <FooterDiv className={styles.siteFooter}>
        <Copyright>
          <span className='ivy'>Isabel Valadez</span> © {new Date().getFullYear()}
          <SelfPromo className='shameless-self-promo'>
            Website by <a href='https://gavinvaught.com' target='_blank' rel="noopener noreferrer">Gavin Vaught</a>
          </SelfPromo>
        </Copyright>
        <nav className={styles.footerNav}>
          {/* LOGO
          <Link to="/">
            <img
              src={footerLogo}
              width="100px"
              alt="ANOUN-logo"
              className={styles.footerLogo}
            />
          </Link>*/}
          <Footernav>
            <li>
              <Link style={{color: '#fcfcfc', 'textDecoration': 'underline', ':hover': 'color: black'}} to="/">Home</Link>
            </li>
            <li>
              <Link style={{color: '#fcfcfc', 'textDecoration': 'underline'}} to="/blog/">Blog</Link>
            </li>
            <li>
              <Link style={{color: '#fcfcfc', 'textDecoration': 'underline'}} to="/about/">About</Link>
            </li>
            <li>
              <Link style={{color: '#fcfcfc', 'textDecoration': 'underline'}} to="/contact/">Contact</Link>
            </li>
          </Footernav>
        </nav>
        <nav className={styles.footerNav2}>
          <a
            href="https://www.instagram.com/newspaperworthy"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              src={instagramIcon}
              className={styles.footerIcon}
              width="18px"
              alt="instagram-social-link"
            />
          </a>
          <a
            href="https://github.com/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              src={githubIcon}
              className={styles.footerIcon}
              width="24px"
              alt="github-social-link"
            />
          </a>
        </nav>
      </FooterDiv>
    )
  }
}

export default Footer
