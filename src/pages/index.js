import React from "react"
import { Link, graphql } from "gatsby"
import Img from "gatsby-image"
import SEO from "../components/SEO/SEO"
import Layout from "../components/Layout/Layout"
import { createGlobalStyle } from "styled-components"
import Button from "@material/react-button"
import Card from "@material/react-card"
// Styles
import "./../styles/app.scss"

export const GlobalStyle = createGlobalStyle`
  &.ivy {
    font-family: 'Playfair Display', sans-serif;
    font-weight: 500;
    font-size: 1.24rem;
  }
  line-height: 1.8rem;
`

class IndexPage extends React.Component {
  render() {
    const { data } = this.props
    const siteTitle = data.site.siteMetadata.title
    const posts = data.allMarkdownRemark.edges
    return (
      <>
        <GlobalStyle />
        <Layout>
          <SEO title={siteTitle}/>
          <section className="anoun-home--section1">
            <h3>
              Hi, I'm <span className="anoun-title">Isabel Valadez</span>
            </h3>
            <br />
            &nbsp;
            <h4>Freelance Writer & Photographer</h4>
            {/*<img src={} alt="anoun-hero" />*/}
            <Link to="/contact/">
              <Button raised className="mdc-button--round">
                Contact
              </Button>
            </Link>
          </section>
          <section className="anoun-home--section2">
            <h3>This is the power statement section</h3>
            <h3>Say something that stands out</h3>
            <h3>Say something visitors will remember</h3>
            <h3>
              Say something <b>bold</b> about your brand
            </h3>
          </section>
          <section className="anoun-home--section3">
            <h2>Blog Posts</h2>
            <div className="blog-posts__container">
              {posts.map(({ node }) => {
                const key = node.fields.slug
                const title = node.frontmatter.title || node.fields.slug
                return (
                  <Link key={ key } to={ node.fields.slug }>
                    <Card
                      className="mdc-card--clickable anoun-blog-card"
                      key={ key }
                    >
                      <Img
                        key={ key }
                        className="mdc-card__media"
                        fluid={
                          node.frontmatter.featured_image.childImageSharp.fluid
                        }
                      />
                      <div className="anoun-blog-card-content__container">
                        <h3>{ title }</h3>
                        <small>{ node.frontmatter.date }</small>
                        <p
                          dangerouslySetInnerHTML={{
                            key: key,
                            __html:
                              node.frontmatter.description || node.excerpt,
                          }}
                        />
                      </div>
                    </Card>
                  </Link>
                )
              })}
            </div>
          </section>
        </Layout>
      </>
    )
  }
}

export default IndexPage
export const indexQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      filter: { fileAbsolutePath: { regex: "/posts/" } }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      edges {
        node {
          id
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            featured_image {
              childImageSharp {
                fluid(maxWidth: 1200, quality: 92) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
  }
`
